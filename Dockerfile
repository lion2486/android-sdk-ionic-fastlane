# FROM runmymind/docker-android-sdk:latest
FROM datencia/ionic-android:latest

# Installing build tools
RUN apt-get update && \
  apt-get install -y \
  wget build-essential

RUN wget -O ruby-install-0.8.3.tar.gz https://github.com/postmodern/ruby-install/archive/v0.8.3.tar.gz && \
    tar -xzvf ruby-install-0.8.3.tar.gz && \
    cd ruby-install-0.8.3/ && make install && ruby-install --system ruby


# Installing fastlane 
RUN gem install rake fastlane bundle


# Work directory
WORKDIR /app

# replacing the static image bellow import 
# FROM opengamer/android-sdk-gradle-fastlane

# ENV PATH $PATH:/opt/gradle/gradle-4.6/bin

# Install nvm & node
# RUN wget -qO- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash
# ENV NVM_DIR=/root/.nvm
# ENV NODE_VERSION=14.19.0
# RUN . "$NVM_DIR/nvm.sh" && nvm install ${NODE_VERSION} && nvm use v${NODE_VERSION} && nvm alias default v${NODE_VERSION}
# ENV PATH="/root/.nvm/versions/node/v${NODE_VERSION}/bin/:${PATH}"

# RUN npm install -g @ionic/cli cordova
# RUN update-alternatives --set java /usr/lib/jvm/java-8-openjdk-amd64/jre/bin/java
# ENV JAVA_HOME /usr/lib/jvm/java-8-openjdk-amd64/
# RUN /opt/android-sdk-linux/tools/bin/sdkmanager --install "ndk;21.0.6113669"
